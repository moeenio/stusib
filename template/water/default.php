<html>

<head>
    <!-- TODO : include title of single posts, pagination? -->
    <title><?= get_option_value("site_title", true) ?></title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="./template/water/assets/css/light.css">
    <link rel="stylesheet" href="./template/water/assets/css/style.css">
</head>

<body>
    <header class="masthead">
        <div class="header-left">
            <?php
            $site_title = get_option_value("site_title", true);
            if (!isset($site_title) || empty($site_title)) {$site_title = "My awesome blog!";}

            $baseurl = get_option_value("baseurl", true);
            if (!isset($baseurl) || empty($baseurl)) {$baseurl = "/";}
            ?>
            <h1 class="site-title"><a href="<?= $baseurl ?>"><?= $site_title ?></a></h1>
            <p class="tagline"><?= htmlspecialchars(get_option_value("site_tagline", true)) ?></p>
        </div>
        <div class="header-right">
            This is the right of the navbar :P
        </div>
    </header>

    <main>
        <?= $content ?>
    </main>

    <footer>Hi</footer>
</body>

</html>
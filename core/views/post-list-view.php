<div class="post-list">

<?php
foreach ($posts as $post) {
?>
    <article class="post">
    <?php
        if (!empty($post["cover_url"])) {
    ?>
        <img class="post-cover" src="<?= $post["cover_url"] ?>" alt="">
    <?php
        }
    ?>
        <h2 class="post-title"><?= $post["title"] ?></h2>
        <div class="post-meta">
            <date class="post-date"><?= $post["date_created_fmt"] ?></date>
        </div>
        <p class="post-excerpt">
        <?php
            if (!$post["summary"] == "") {
                echo $post["summary"];
            }
            else {
                echo strip_tags($post["content_html"], "<br><strong><em>");
            }
        ?>
        </p>
        <a class="post-read-more" href="./?post_id=<?= $post["id"] ?>">Read more &rarr;</a>
    </article>

<?php
}
?>

<?php
// TODO : implement rel="next"/"prev" somehow?
// Put pagination controls at the top on pages other than 1?

if ($page > 1) {
?>
<div class="pagination-left">
    <!--<a class="pagination-first" href="?page=1">Newest posts</a>-->
    <a class="pagination-prev" href="?page=<?=$page-1?>">&larr; Newer posts</a>
</div>
<?php
}

if ($page < $page_count) {
?>
<div class="pagination-right">
    <a class="pagination-next" href="?page=<?=$page+1?>">Older posts &rarr;</a>
    <!--<a class="pagination-last" href="?page=<?=$page_count?>">Oldest posts</a>-->
</div>
<?php
}
?>
</div>

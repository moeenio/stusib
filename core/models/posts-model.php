<?php

function get_post($id)
{
    global $database;
    $parsedown = new Parsedown();
    $parsedown->setBreaksEnabled(true);

    $date_format = get_option_value("date_format", true);
    if (!isset($date_format) || empty($date_format)) {$date_format = "l, F d, Y";}

    try {
        $query = $database->prepare("SELECT * FROM posts WHERE id = ?");
        $query->execute(array($id));
        $post = $query->fetch();
        if (!$post) {
            $query->closeCursor();
            return NULL;
        }

        else {
            $post = array_merge($post, [
                "date_created_fmt" => date($date_format, $post["date_created"]),
                "date_modified_fmt" => date($date_format, $post["date_modified"]),
                "content_html" => $parsedown->text($post["content"])
            ]);
            $query->closeCursor();
            return $post;
        }
    }
    catch (Exception $e) {
        die("Looks like something is wrong with the database:\n" . $e->getMessage());
    }
}

function get_posts($perpage = NULL, $page = NULL)
{
    global $database;
    $parsedown = new Parsedown();
    $parsedown->setBreaksEnabled(true);

    $date_format = get_option_value("date_format", true);
    if (!isset($date_format) || empty($date_format)) {$date_format = "l, F d, Y";}

    try {
        /*$query = $database->query("SELECT id, title, cover_url, summary, content,
    STRFTIME('%W, %M %D', `date_created`) AS date_created_fmt FROM posts");*/
        $sql = "SELECT * FROM posts ORDER BY date_created DESC";
        $params = array();
        if (isset($perpage)) {
            $sql .= " LIMIT ?";
            $params[] = $perpage;

            if (isset($page)) {
                $sql .= " OFFSET ?";
                $params[] = ($page-1)*$perpage;
            }
        }
        $query = $database->prepare($sql);
        $query->execute($params);
        $posts = $query->fetchAll();
        if (!$posts) {
            $query->closeCursor();
            return NULL;
        }
        else {
            foreach ($posts as &$post) {
                $post = array_merge($post, [
                    "date_created_fmt" => date($date_format, $post["date_created"]),
                    "date_modified_fmt" => date($date_format, $post["date_modified"]),
                    "content_html" => $parsedown->text($post["content"])
                ]);
            }

            unset($post);
            $query->closeCursor();
            return $posts;
        }
    }
    catch (Exception $e) {
        die("Looks like something is wrong with the database:\n" . $e->getMessage());
    }
}

function get_page_count($perpage) {
    global $database;

    $query = $database->query("SELECT COUNT(*) FROM posts");
    $post_count = $query->fetchColumn();
    $page_count = ceil($post_count / $perpage);

    return $page_count;
}